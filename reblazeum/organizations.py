#!/usr/bin/python
# -*- coding: UTF-8 -*-

from mongoengine import *
from datetime import datetime

connect("reblaze_users_db")

class Organizations(Document):
	email = EmailField(required=True)
	company_name = StringField(required=True)
	org_admins = ListField(required=True)
	address = StringField(required=True)
	mobile = StringField(required=True)

def get_org_by_id(_id):
	org = Organizations.objects.get(id=_id)
	return dict(org.to_mongo())

def get_all_orgs():
	accs = Organizations.objects()
	result = []
	for org in orgs:
		result.append(dict(org.to_mongo()))
	return result

def create_org(doc):
	new_org = Organizations(**doc).save()
	return dict(new_org.to_mongo())

def delete_org(_id):
	return Organizations.objects.get(id=_id).delete()

def update_org(_id, doc):
	return Organizations.objects(id=_id).update(**doc)