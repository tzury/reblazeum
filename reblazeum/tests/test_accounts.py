from accounts import *

class TestClass(object):

	def setup_class(self):

		self.valid_create_account_doc = {
			"email": "mazal@reblaze.com",
			"yubikeyid": [
				"cccinjdhfngjfg",
				"cccinjdhrjfmfk",
			],
			"otpseed": "JSHJKNUDJKSHNRXP",
			"contact_name": "Mazal Cohen",
			"company_name": "reblaze",
			"mobile": "+972544212123",
			"access_level": "1",
			"password": "TtEKYWAa6NHwdzoR/tKKYWAa6NHwdzg/ErCHS16b\nzDd30w==",
			# "authentication": {
			#     "yubikeyid": [
			#     "cccinjdhfngjfg",
			#     "cccinjdhrjfmfk",
			#     ],
			#     "otpseed": "JSHJKNUDJKSHNRXP"
			# },
			# "org_id": "58fc9b7a55935d101cc42d22",
			# "password_age": datetime.now(),
			# "password_history": [
			#     "TtEbgSpQ22y78oR/tKBhYg/ErVgT9qpsdCpnxQoSBC3HS016b\nzDd30w==\n",
			#     "IIT92rZHgL8f8jf8kKNlxuUNO7yWCIH6OKYWAa6NHwdzkUzUPs3ZBznT8RQ=="
			# ],
		}

		self.valid_fields = {
			"email": "mazal@reblaze.com",
			"yubikeyid": [
				"cccinjdhfngjfg",
				"cccinjdhrjfmfk",
			],
			"otpseed": "JSHJKNUDJKSHNRXP",
			"contact_name": "Mazal Cohen",
			"company_name": "reblaze",
			"mobile": "+972544212123",
			"access_level": "1",
			"password": "TtEKYWAa6NHwdzoR/tKKYWAa6NHwdzg/ErCHS16b\nzDd30w=="
		}

		self.invalid_fields = {
			"email": "mazalreblaze.com",
			"yubikeyid": 'agdadagadhadh',
			"otpseed": 101331,
			"contact_name": False,
			"company_name": 492752,
			"mobile": 972544212123246246,
			"access_level": 100,
			"password": 123456,
		}

		self.valid_update_doc = {
			"email": "mazalmazi@reblaze.com",
			"contact_name": "Mazal Mazi Cohen",
			"company_name": "reblaze",
			"mobile": "+9725435667123",
			"access_level": "10",
		}

		self.invalid_update_doc = {
			"email": "mazal@reblaze.com",
			"yubikeyid": [
				"cccinjdhfngjfg",
				"cccinjdhrjfmfk",
			],
			"otpseed": "JSHJKNUDJKSHNRXP",
			"contact_name": "Mazal Cohen",
			"company_name": "reblaze",
			"mobile": "+972544212123",
			"access_level": "1",
			"password": "TtEKYWAa6NHwdzoR/tKKYWAa6NHwdzg/ErCHS16b\nzDd30w==",
			"failed_field": True
		}

		# create account for get all accounts test
		self.new_account = create_account(self.valid_create_account_doc)
		self.new_account_id = self.new_account['_id']

		# update account with invalid fields



	def teardown_class(self):
		# delete the account made for get all accounts
		delete_account(self.new_account_id)

	def test_get_account_by_id_valid(self):
		try:
			account = get_account_by_id(self.new_account_id)
			assert account['_id']
			assert True
		except:
			assert False

	def test_get_all_accounts_valid(self):
		all_accounts = get_all_accounts()
		assert type(all_accounts) is list
		assert len(all_accounts) > 0

	def test_create_delete_account_valid(self):
		new_account = create_account(self.valid_create_account_doc)
		assert new_account["_id"]

		try:
			delete_account(new_account["_id"])
			assert True
		except:
			assert False


	def test_create_account_invalid(self):
		invalid_doc = self.valid_create_account_doc

		for field in self.valid_create_account_doc.keys():
			try:
				invalid_doc[field] = self.invalid_fields[field]
				new_account = create_account(invalid_doc)
				assert False
			except:
				assert True
				invalid_doc[field] = self.valid_fields[field]


	def test_delete_account_invalid(self):
		try:
			delete_account('5ef4a314c88db5948b40713f')
			assert False
		except:
			assert True

	def test_update_account_valid(self):
		updated_account = update_account(self.new_account_id, self.valid_update_doc)
		account = get_account_by_id(self.new_account_id)
		assert account['email'] == self.valid_update_doc['email']


	def test_update_account_invalid(self):
		invalid_doc = self.valid_create_account_doc

		for field in self.valid_create_account_doc.keys():
			try:
				invalid_doc[field] = self.invalid_fields[field]
				new_account = update_account(self.new_account_id, invalid_doc)
				assert False
			except:
				assert True
				invalid_doc[field] = self.valid_fields[field]

		try:
			update_account(self.new_account_id, self.invalid_update_doc)
			assert False
		except:
			assert True





