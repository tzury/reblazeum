#!/usr/bin/python
# -*- coding: UTF-8 -*-

from mongoengine import *
from datetime import datetime

connect("reblaze_users_db")

class Accounts(Document):
    email = EmailField(required=True)
    contact_name = StringField(required=True)
    company_name = StringField(required=True)
    otpseed = StringField()
    yubikeyid = ListField()
    mobile = StringField(required=True)
    access_level = StringField(required=True)
    password = StringField(required=True)
    # org_id = StringField(required=True)
    # authentication = DictField(required=True)
    # password_age = DateTimeField(required=True)
    # password_history = ListField(StringField(), default=list, required=True)

def get_account_by_id(_id):
    acc = Accounts.objects.get(id=_id)
    return dict(acc.to_mongo())

def get_all_accounts():
    accs = Accounts.objects()
    result = []
    for acc in accs:
        result.append(dict(acc.to_mongo()))
    return result

def create_account(doc):
    new_account = Accounts(**doc).save()
    return dict(new_account.to_mongo())

def delete_account(_id):
    return Accounts.objects.get(id=_id).delete()

def update_account(_id, doc):
    return Accounts.objects(id=_id).update(**doc)


